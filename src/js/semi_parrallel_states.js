import { interact_service } from './semi_parallel_states/player_interactions'
import { node_service } from './semi_parallel_states/node_states'

node_service.start()
interact_service.start()

const semiParallel = document
.querySelector('.semi-parallel')
.querySelectorAll('button')
.forEach((button)=>{
    button.addEventListener('click',()=>{
        interact_service.send(button.dataset.send)
        if( button.dataset.send === 'RIGHT' || 
            button.dataset.send === 'WRONG')
        {
            node_service.send(button.dataset.send)
        }
        console.log(node_service.state.value,interact_service.state.value)
    })
})

node_service.stop()
interact_service.stop()