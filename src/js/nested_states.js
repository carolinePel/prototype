import {service} from'./linked_states/link_states'

service.start()

const init = document
.querySelector('.linked')
.querySelectorAll('button')
.forEach((button)=>{
    button.addEventListener('click',()=>{
        service.send(button.dataset.send)
        console.log("click", service.state.value, service.state.context)
    })
})
service.stop()
