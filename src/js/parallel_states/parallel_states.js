import { Machine, actions as lActions } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';
import { interaction_config } from '../states/interaction_states/interaction_state'
import { node_config } from '../states/node_states/nodes_state'
import { player_config } from '../states/player_states/player_state'
const { send, assign } = lActions

const player = {
        initial:"paused",
        states: {
            playing: {
                on: {
                    PAUSE: "paused",
                    BLOCK: "blocked"
                }
            },
            paused: {
                on: {
                    PLAY: "playing",
                    BLOCK: "blocked"
                }
            },
            blocked:{
                type:'final'
            }
        },
    }

const interaction = {
    initial:"hiding",
    states:{
        hiding:{
            on:{
                BLOCK:'showing'
            }
        },
        showing: {
        on: {
            RIGHT: {
                target:'end',
                actions:[
                    assign({isUserRight:true}),
                    'logContext'
                ]
            },
            WRONG: 'end',
            },
        },
        end:{
            type:'final',
        },
    },
}


export const full_config = {
    initial:'node_1',
    context:{
        isUserRight: false,
    },
    states:{
        node_1: {
            type:"parallel",
            states:{
                player,
                interaction,
            },
            onDone:[
                {
                    target:"node_3",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
        node_2: {
            type:"parallel",
            states:{
                player,
                interaction,
            },
            onDone:[
                {
                    target:"node_1",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_3",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
        node_3: {
            type:"parallel",
            states:{
                player,
                interaction,
            },
            onDone:[
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
    }
}

const full_actions = {
    logContext:    (ctx,ev)=>console.log(ctx),
    setContextTrue:            assign({isUserRight:true}),
    setContextFalse:           assign({isUserRight:false}),
}

export const nodePlayerMachine = Machine({...full_config},{actions:full_actions})
export const service = interpret(nodePlayerMachine)
  .onTransition(nextState => {
    console.log("parallel", nextState.value)
  })
//   console.log('full',full_config)
//   service.start()
//   service.send("PLAY")
//   service.send("PAUSE")
//   service.send("PLAY")
//   service.send("BLOCK")
//   service.send("WRONG")

// //interaction
// service.send("RIGHT")
// service.send("PLAY")
// service.send("PAUSE")
// service.send('BLOCK')
// service.send("WRONG")
// service.stop()
