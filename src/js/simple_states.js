import { interact_service } from './simple_states/simple_states'
import { node_service } from './simple_states/simple_states'

node_service.start()
interact_service.start()

const simple = document
.querySelector('.simple')
.querySelectorAll('button')
.forEach((button)=>{
    button.addEventListener('click',()=>{
        interact_service.send(button.dataset.send)
        if( button.dataset.send === 'RIGHT' || 
            button.dataset.send === 'WRONG')
        {
            node_service.send(button.dataset.send)
        }
        console.log(node_service.state.value,interact_service.state.value)
    })
})

node_service.stop()
interact_service.stop()