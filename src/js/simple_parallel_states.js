import { parallel_service } from './simple_states/simple_parallel_states'

parallel_service.start()

const init = document
.querySelector('.simple-parallel')
.querySelectorAll('button')
.forEach((button)=>{
    button.addEventListener('click',()=>{
        parallel_service.send(button.dataset.send)
        console.log(parallel_service.state.value)
    })
})
parallel_service.stop()