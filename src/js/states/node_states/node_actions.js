import { interpret } from 'xstate/lib/interpreter'
import {node_Machine} from './nodes_state'

export const node_service = interpret(node_Machine)
node_service.onTransition(nextState => {
    //console.log("next state value:", nextState.value)
})
node_service.start()


const n_buttons = document.querySelector('.node').querySelectorAll('button')
n_buttons.forEach((button)=>{
    button.addEventListener('click',()=>{   
        node_service.send(`${button.dataset.send}`)
        console.log(node_service.state.value) 
    })
})



node_service.stop()