import { Machine } from 'xstate';

export const node_state = {
    //noeud vidéo simple
    node_1: {
        //onEntry:["logEntry"],
        on: {
            NEXT_T: {
                target:"node_3",
                //actions:"logNextTrue"
            },
            NEXT_F: {
                target:"node_2",
                //actions:"logNextFalse"
            }
        },
    },
    node_2: {
        //onEntry:["logEntry"],
        on: {
            NEXT_T: {
                target:"node_1",
                //actions:"logNextTrue"
            },
            NEXT_F: {
                target:"node_3",
                //actions:"logNextFalse"
            }
        },
    },
    node_3: {
        //onEntry:["logEntry"],
        on: {
            NEXT_T: {
                target:"node_2",
                //actions:"logNextTrue"
            },
            NEXT_F: {
                target:"node_2",
                //actions:"logNextFalse"
            }
        },
    },
}

export const nodeActions = {
    logNextTrue:  () => console.log("one node: next true"),
    logNextFalse: () => console.log("one node: next false"),
    logEntry:     () => console.log("one node: entrying new node"),
    logFinalEntry:() => console.log("one node: final entry")
}



export const node_config = {
    initial: "node_1",
    states: node_state,
}



export const node_Machine = Machine(node_config,{actions:nodeActions})
