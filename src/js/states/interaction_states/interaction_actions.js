import { interpret } from 'xstate/lib/interpreter'
import {interactionMachine} from './interaction_state'

export const interaction_service = interpret(interactionMachine)
interaction_service.onTransition(nextState => {
    //console.log("next state value:", nextState.value)
})

interaction_service.start()


const i_buttons = document.querySelector('.interaction').querySelectorAll('button')
i_buttons.forEach((button)=>{
    button.addEventListener('click',()=>{    
        interaction_service.send(`${button.dataset.send}`)
    })
})



interaction_service.stop()