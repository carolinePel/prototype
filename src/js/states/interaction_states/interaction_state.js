import { Machine } from 'xstate'
import { interpret } from 'xstate/lib/interpreter';


//todo
export function isCompleted(ctx, stateValue){

  return false
}

export const interaction_config = {
    initial: 'interaction',
    states: {
      interaction: {
        initial: 'hiding',
        on: {
          '': {
            target: 'completed',
            cond: (ctx, eventObj, stateValue) => isCompleted(ctx, stateValue),
          },
        },
        states: {
          hiding:{
            //onEntry: ['logHiding'],
            on:{
              SHOW:'showing'
            }
          },
          showing: {
            //onEntry: ['logShowing'],
            on: {
              RIGHT: 'right',
              WRONG: 'wrong',
            },
          },

          right: {
            //onEntry: ['logRight'],
            on:{
              HIDE:'hiding'
            }
            // after: {
            //   7000:{target:'hiding',}
            // }
          },

          wrong: {
            //onEntry: ['logWrong'],
            // after: {
            //   7000:{target:'hiding',}
            // }
            on:{
              HIDE:'hiding'
            }
          },
        },
      },
  
      completed: {
        onEntry: {
          target:'interaction.right',
          //actions:['logCompleted']
        },
      },
    },
  }
  
export const interaction_actions = {
    "logHiding": (ctx,ev) => {console.log('hiding')},
    "logShowing": (ctx,ev) => {console.log('showing')},
    "logRight": (ctx,ev) => {console.log('right')},
    "logWrong": (ctx,ev) => {console.log('wrong')},
    "logCompleted":()=>{console.log('completed')}
  }

export const interactionMachine = Machine({...interaction_config}, {actions:interaction_actions})

// const service = interpret(interactionMachine)
//   .onTransition(nextState => {
//     console.log("value",nextState.value);
//   })

// service.start()
// service.send("RIGHT")

// service.stop()