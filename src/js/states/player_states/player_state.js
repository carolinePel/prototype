import { Machine } from 'xstate';

export const states = {

  playing: {
    //onEntry:['logPlaying'],
    on: {
      PAUSE: "paused",
      BLOCK: "blocked"
    }
  },
  paused: {
    //onEntry:['logPaused'],
    on: {
      PLAY: "playing",
      BLOCK: "blocked"
    }
  },
  blocked:{
    //onEntry:['logBlocked'],
    on:{
      UNBLOCK: "playing",
    }
  }
}

export const playerActions = {
  "logPlaying": (ctx, ev) => {console.log('playing')},
  "logPaused": (ctx,ev) => {console.log("pausing")},
  "logBlocked": ()=>{console.log('blocked')}
}

export const player_config = {
  id:"playerStates",
  initial:"paused",
  states,
}

export const playerMachine = Machine(player_config,{actions:playerActions})
