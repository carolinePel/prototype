import { interpret } from 'xstate/lib/interpreter'
import { playerMachine } from './player_state'

export const player_service = interpret(playerMachine)
player_service.onTransition(nextState => {
    //console.log("next state value:", nextState.value)
})

player_service.start()


const buttons = document.querySelector('.player').querySelectorAll('button')
buttons.forEach((button)=>{
    button.addEventListener('click',()=>{    
        player_service.send(`${button.dataset.send}`)
    })
})



player_service.stop()