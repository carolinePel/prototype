import { Machine, actions as lActions } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';
const { send, assign } = lActions

const player = {
        initial:"paused",
        states: {
            playing: {
                on: {
                    PAUSE: "paused",
                    BLOCK: "blocked"
                }
            },
            paused: {
                on: {
                    PLAY: "playing",
                    BLOCK: "blocked"
                }
            },
            blocked:{
                on:{
                    RIGHT:"playing",
                    WRONG:"playing"
                }
            },
        },
    }

const interaction = {
    initial:"hiding",
    states:{
        hiding:{
            on:{
                BLOCK:'showing'
            }
        },
        showing: {
            on: {
                RIGHT: {
                    target:'hiding',
                },
                WRONG: 'hiding',
            },
        },
    },
}

export const full_config = {
    type:'parallel',
    context:{
        isUserRight: false,
    },
    states:{
        player,
        interaction,
    },
}

export const player_interact_Machine = Machine({...full_config})
export const interact_service = interpret(player_interact_Machine)
  .onTransition(nextState => {
    console.log("semi-parallel", nextState.value)
  })