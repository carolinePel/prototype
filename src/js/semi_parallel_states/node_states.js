import { Machine } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';

export const node_state = {
    node_1: {
        on: {
            RIGHT: {
                target:"node_3",
            },
            WRONG: {
                target:"node_2",
            }
        },
    },
    node_2: {
        on: {
            RIGHT: {
                target:"node_1",
            },
            WRONG: {
                target:"node_3",
            }
        },
    },
    node_3: {
        on: {
            RIGHT: {
                target:"node_2",
            },
            WRONG: {
                target:"node_2",
            }
        },
    },
}

export const node_config = {
    initial: "node_1",
    states: node_state,
}

export const node_Machine = Machine(node_config)
export const node_service = interpret(node_Machine)
  .onTransition(nextState => {
    console.log("semi-parallel", nextState.value)
  })