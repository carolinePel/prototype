import { player_service } from './states/player_states/player_actions'
import { interaction_service } from './states/interaction_states/interaction_actions'
import { node_service } from './states/node_states/node_actions'

/**
 * PROTOTYPE WITH DISCONNECTED COMPONENTS
 */

node_service.start()
const automaticDiv = document
.querySelector('.automatic')

const playbtn = automaticDiv
.querySelector('.start')
.addEventListener('click',()=>{
    player_service.send('PLAY')
    console.log("independent",node_service.state.value,interaction_service.state.value,player_service.state.value)                
})

const blockbtn = automaticDiv
.querySelector('.block')
.addEventListener('click',()=>{
    player_service.send('BLOCK')
    interaction_service.send("SHOW")
    console.log("independent",node_service.state.value,interaction_service.state.value,player_service.state.value)                
})

const rightbtn = automaticDiv
.querySelector('.right')
.addEventListener('click',()=>{
    //feedBack screen
    interaction_service.send('RIGHT')
    //node change
    interaction_service.send('HIDE')
    node_service.send('NEXT_T')
    player_service.send('UNBLOCK')
    console.log("independent",node_service.state.value,interaction_service.state.value,player_service.state.value)                
})

const wrongbtn = automaticDiv
.querySelector('.right')
.addEventListener('click',()=>{
    //feedBack screen
    interaction_service.send('WRONG')
    //node change
    interaction_service.send('HIDE')
    node_service.send('NEXT_T')
    player_service.send('UNBLOCK')
    console.log("independent",node_service.state.value,interaction_service.state.value,player_service.state.value)                
})

node_service.stop()