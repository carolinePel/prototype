import {service} from'./parallel_states/parallel_states'

service.start()

const init = document
.querySelector('.parallel')
.querySelectorAll('button')
.forEach((button)=>{
    button.addEventListener('click',()=>{
        service.send(button.dataset.send)
        console.log(service.state.value)
    })
})
service.stop()