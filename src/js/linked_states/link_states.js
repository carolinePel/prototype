import { Machine, actions as lActions } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';
import { interaction_config } from '../states/interaction_states/interaction_state'
import { node_config } from '../states/node_states/nodes_state'
import { player_config } from '../states/player_states/player_state'
const { send, assign } = lActions

const playerState = {
    player:{
        initial:"paused",
        states: {
            playing: {
                on: {
                    PAUSE: "paused",
                    BLOCK: "blocked"
                }
            },
            paused: {
                on: {
                    PLAY: "playing",
                    BLOCK: "blocked"
                }
            },
            blocked:{
                type:'final'
            }
        },
        onDone:{
            target:'interaction'
        }
    }
}
const interactionState = {
    interaction:{
        initial:"showing",
        states:{
            showing: {
            on: {
                RIGHT: {
                    target:'end',
                    actions:[
                        assign({isUserRight:true}),
                        'logContext'
                    ]
                },
                WRONG: 'end',
                },
            },
            end:{
                type:'final',
            },
        },
        onDone:{
            target:'changeNode'
        }
    },
}
const changeNode = {
    changeNode:{
        type:"final"
    }
}

export const full_config = {
    initial:'node_1',
    context:{
        isUserRight: false,
    },
    states:{
        node_1: {
            initial:"player",
            states:{
                ...playerState,
                ...interactionState,
                changeNode:{
                    type:"final"
                }
            },
            onDone:[
                {
                    target:"node_3",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
        node_2: {
            initial:"player",
            states:{
                ...playerState,
                ...interactionState,
                changeNode:{
                    type:"final"
                }
            },
            onDone:[
                {
                    target:"node_1",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_3",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
        node_3: {
            initial:"player",
            states:{
                ...playerState,
                ...interactionState,
                changeNode:{
                    type:"final"
                }
            },
            onDone:[
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return ctx.isUserRight
                    },
                },
                {
                    target:"node_2",
                    cond:(ctx,event)=>{
                        return !ctx.isUserRight
                    },
                },
            ],
        },
    }
}

const full_actions = {
    logContext:    (ctx,ev)=>console.log(ctx),
    setContextTrue:            assign({isUserRight:true}),
    setContextFalse:           assign({isUserRight:false}),
}

export const nodePlayerMachine = Machine({...full_config},{actions:full_actions})
export const service = interpret(nodePlayerMachine)
  .onTransition(nextState => {
    console.log("linked", nextState.value)
  })

//   service.start()
//   service.send("PLAY")
//   service.send("PAUSE")
//   service.send('BLOCK')

// //interaction
// service.send("RIGHT")
// service.send("PLAY")
// service.send("PAUSE")
// service.send('BLOCK')
// service.send("WRONG")
//   service.stop()
