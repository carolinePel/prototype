import { Machine } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';

export const node_state = {
    node_1: {
        on: {
            RIGHT:"node_3",
            WRONG: "node_2",
        },
    },
    node_2: {
        on: {
            RIGHT: {
                target:"node_1",
            },
            WRONG: {
                target:"node_3",
            }
        },
    },
    node_3: {
        on: {
            RIGHT: {
                target:"node_2",
            },
            WRONG: {
                target:"node_2",
            }
        },
    },
}

export const node_config = {
    initial: "node_1",
    states: node_state,
}

const interactions = {
    initial:"playPause",
    states:{
        playPause:{
            on:{
                BLOCK:'showing'
            }
        },
        showing: {
            on: {
                RIGHT: 'playPause',
                WRONG: 'playPause',
            },
        },
    },
}

export const node_Machine = Machine(node_config)
export const node_service = interpret(node_Machine)
  .onTransition(nextState => {
    console.log("simple", nextState.value)
  })

export const interact_Machine = Machine(interactions)
export const interact_service = interpret(interact_Machine)
.onTransition(nextState => {
    console.log("simple", nextState.value)
})