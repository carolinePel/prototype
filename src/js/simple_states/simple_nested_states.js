import { Machine, actions as lActions  } from 'xstate';
import { interpret } from 'xstate/lib/interpreter';
const { assign } = lActions




//console.log("increase context",increaseContext())


/**
 * Create a state that reacts to 2 events
 * @param {Object} config 
 */


function makeDualEventState (config = {}){
  const { success = "", error = "" } = config
  return {
    on: {
      SUCCESS:{
        target: success,
        action:assign({})
      },
      ERROR: {
        target: error,
      },
    }
  }
}
//console.log("make dual event state", makeDualEventState())

//*
function setCurrentContext({node, context, event}){
  //console.log("set current context",context, event, node)
  return node ? [
    assign((context,event) => increaseContext({context, node, event})),
  ] : []
}


//console.log("react to event",reactToEvent('node_2'))
/* */


function makeLinearState(config = {}){
  const { event = '', target = ''  } = config
  const on = {}
  on[event] = target
  
  return {on}
}
console.log("linearState", makeLinearState({event: "yolo", target: "pouet"}))

/**
 * Create an onDone object with guard
 * @param {Object} config 
 */

function makeOnDone(config = {}){
  const { targets = { a: "", b: ""}, node = "" } = config

  return [
    {
      target: targets.a,
      cond: (context,event) => {
        return context[node].content.type  
      }
    },
    {
      target: targets.b,
      cond:(context,event) => {
          return !context[node].content.type
      }
    }
  ]
}

/**
 * Event is sent each time a person clicks on a checkbox,
 * the function gets the answer in the context, 
 * checks if answer is complete, 
 * and changes state if it is
 * @param {Object} config 
 */
function makeLoopEvent(config = {}){
  const { node = "", name = "", target = "" } = config
  const event = {}
  event[name] = [
    { 
      actions: assign((context, event) => increaseContext({context, event, node})),
      cond: (context, event) => !isAnswerComplete(context, event)
    },
    { 
      actions: assign((context, event) => increaseContext({context, event, node})),
      target: target,  
      cond: (context, event) => isAnswerComplete(context, event)
    },
  ]
  return {
      on:{...event}
  }
}
console.log("make loop state",makeLoopEvent({node:"node_1", name:"ANSWER", target:"answered"}))


/**
 * Add new items to existing context
 * value is expected to have the following form
 * value:{ answer: [], total: 0 }
 * @param {Object} config 
 */
function increaseContext (config = {}){
  const { context = {}, node = '', event = { type: "" } } = config
  console.log("config",config)
  const value = event.value ? event.value.answer : null
  if(node){
    context[node] = {
      content: {
        isCorrect:false,
        value
      }
    }
  }

  return context
}

function isAnswerComplete(context, event){
  const { answer, total } = event.value
  
  return answer.length === total
}
/**
 * nodes
 */

const node_1 = {
  initial:"play",
  states: {
    play:   makeLinearState({ event: "INTERACT", target: 'interact' }),
    interact: makeLoopEvent({ node: "node_1", name: "ANSWER", target: "answered"}),
    answered: makeDualEventState({ node:"node_1", success: "end", error: "end"} ),
    end : { type: "final" }
  },
  onDone:
    makeOnDone({ targets: { a: "node_3", b: "node_2" }, node: "node_1"}),
}
console.log(node_1)

const node_2 = {
  initial:"play",
  states: {
    play:   makeLinearState({ event: "INTERACT", target: 'interact' }),
    interact: makeLoopEvent({ node: "node_2", event: "ANSWER", target: "answered"}),
    answered: makeDualEventState({ node: "node_2", success: "end", error: "end"} ),
    end : { type: "final" }
  },
  onDone:
  makeOnDone({ targets: { a: "node_1", b: "node_3" }, node: "node_2"}),
}

const node_3 = {
  initial:"play",
  states: {
    play:   makeLinearState({ event: "INTERACT", target: 'interact' }),
    interact: makeLoopEvent({ node: "node_3", event: "ANSWER", target: "answered"}),
    answered: makeDualEventState({ node: "node_3", success: "end", error: "end"} ),
    end : { type: "final" }
  },
  onDone:
  makeOnDone({ targets: { a: "node_4", b: "node_1" }, node: "node_3"}),
}

const node_4 = {
  initial: "showing",
  states: {
    showing: makeDualEventState({ success: "end", error: "end" }),
    end : {type:"final"}
  },
  onDone: 
  makeOnDone({ targets: { a: "node_5", b: "node_5" }, node: "node_4"}),
}

const node_5 = {
  initial:"video",
  states:{
    video: makeLinearState({ event: 'INTERACT', target: 'end' }),
    end : {type:"final"}
  },
  onDone:{
    target:"",
  }
}



export const full_state = {
    initial:"node_1",
    context:{},
    states:{
      node_1,
      node_2,
      node_3,
      node_4,
      node_5
    }
}
console.log("full state",full_state)
const full_actions = {
  logsmth: (ctx,event) => console.log('logsmth',ctx,event.value),
}

export const full_Machine = Machine(full_state,{actions:full_actions})

export const full_service = interpret(full_Machine)
.onTransition(nextState => {
  //console.log("simple_nested", nextState.value, nextState.context)
})

